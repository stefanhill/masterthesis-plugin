package uni_ko.bpm.cockpit.Simulation;

import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.ProcessEngine;
import org.camunda.bpm.engine.ProcessEngines;
import org.camunda.bpm.engine.identity.User;

import java.util.ArrayList;
import java.util.List;

public class UserManagement {
    protected ProcessEngine processEngine;
    protected IdentityService identityService;

    public UserManagement() {
        this.processEngine = ProcessEngines.getDefaultProcessEngine();
        this.identityService = processEngine.getIdentityService();
    }

    public List<User> addUsers(List<String> names) {
        List<User> users = new ArrayList<>();
        for (String name :
                names) {
            users.add(this.identityService.newUser(name));
        }
        return users;
    }

}
