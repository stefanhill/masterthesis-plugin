package uni_ko.PythonAdapter.MachineLearning;

import uni_ko.bpm.Data_Management.Data_Set;
import uni_ko.bpm.Machine_Learning.Classification;
import uni_ko.bpm.Machine_Learning.Metric;
import uni_ko.bpm.Machine_Learning.PredictionType;
import uni_ko.bpm.Machine_Learning.Util.Parameter_Communication_Wrapper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class XGBoost extends PythonAdapterBaseClassifier {

    public XGBoost() {
        super();
    }

    @Override
    public Metric train(Data_Set trainingSet, Data_Set testSet) throws Exception {
        return this.send_training(trainingSet, testSet);
    }

    @Override
    public List<Classification> evaluate(Data_Set data_set) throws Exception {
        return this.send_evaluate(data_set);
    }

    @Override
    public List<Parameter_Communication_Wrapper> configurational_parameters() {
        this.classifier_class_python = "XGBoost";
        List<Parameter_Communication_Wrapper> parameters = super.get_intercase_configurational_params();
        return parameters;
    }

    @Override
    public void set_configurational_parameters(HashMap<Integer, Object> parameters) {
        super.set_intercase_configurational_parameters(parameters);
    }
}