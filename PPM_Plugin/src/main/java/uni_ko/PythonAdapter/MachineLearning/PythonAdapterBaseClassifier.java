package uni_ko.PythonAdapter.MachineLearning;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.MapType;
import uni_ko.PythonAdapter.MachineLearning.Metric.PythonAdapterMetric;
import uni_ko.PythonAdapter.PythonAdapterConfiguration;
import uni_ko.bpm.Data_Management.Data_Set;
import uni_ko.bpm.Machine_Learning.Classification;
import uni_ko.bpm.Machine_Learning.Classifier;
import uni_ko.bpm.Machine_Learning.Metric;
import uni_ko.bpm.Machine_Learning.PredictionType;
import uni_ko.bpm.Machine_Learning.Util.Parameter_Communication_Wrapper;
import uni_ko.bpm.Machine_Learning.Util.Parameter_Communication_Wrapper_Lists;
import uni_ko.bpm.Machine_Learning.Util.Parameter_Communication_Wrapper_Single;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.*;

import static java.util.stream.Collectors.toList;

public abstract class PythonAdapterBaseClassifier extends Classifier {

    private List<Object> unique_task_names = new ArrayList<>();
    protected String classifier_class_python = "";
    protected Integer intracase_steps;
    protected List<String> intercase_encoders;
    protected Float intercase_timedelta;

    public List<Parameter_Communication_Wrapper> get_intercase_configurational_params() {
        List<Parameter_Communication_Wrapper> parameters = new ArrayList<>();
            parameters.add(new Parameter_Communication_Wrapper_Lists<PredictionType>(
                    0,
                    "Prediction-Types",
                    "Defines which types this classifier will predict.",
                    false,
                    this.prediction_types,
                    PredictionType.class,
                    Arrays.asList(PredictionType.ActivityPrediction, PredictionType.InterCase),
                    true)
            );
            parameters.add(new Parameter_Communication_Wrapper_Single<Integer>(
                    1,
                    "Intracase-Steps",
                    "Steps to be included during the intracase encoding phase.",
                    false,
                    this.intracase_steps,
                    Integer.class,
                    4)    // suggested value is 4
            );
            parameters.add(new Parameter_Communication_Wrapper_Lists<String>(
                            2,
                            "Intercase-Encoders",
                            "Defines which intercase encoders during the feature encoding process, if intercase prediction is activated.",
                            false,
                            this.intercase_encoders,
                            String.class,
                            Arrays.asList("NoPeerCases", "PeerActivityCount"),
                            true
                    )
            );
            parameters.add(new Parameter_Communication_Wrapper_Single<Float>(
                    3,
                    "Intercase-Timedelta",
                    "Factor to be multiplied with the median case length which determines the size of the peer case window.",
                    false,
                    this.intercase_timedelta,
                    Float.class,
                    0.5F)    // suggested value is 0.5F
            );
        return parameters;
    }

    public void set_intercase_configurational_parameters(HashMap<Integer, Object> parameters) {
        try {
            this.prediction_types = ((List<String>) parameters.get(0)).stream()
                    .map(PredictionType::valueOf)
                    .collect(toList());
        } catch (Exception e) {
            this.prediction_types = ((List<PredictionType>) parameters.get(0));
        }
        this.intracase_steps = Integer.parseInt(parameters.get(1).toString());
        this.intercase_encoders = ((List<String>) parameters.get(2));
        this.intercase_timedelta = Float.parseFloat(parameters.get(3).toString());
    }

    @Override
    public List<PredictionType> get_prediction_type() {
        List<PredictionType> prediction_types = new ArrayList<>();
        prediction_types.add(PredictionType.ActivityPrediction);
        prediction_types.add(PredictionType.InterCase);
        return prediction_types;
    }

    public String get_classifier_python_name() {
        return this.get_given_name();
    }

    public Metric send_training(Data_Set training_set, Data_Set test_set) throws Exception {

        HttpClient httpClient = HttpClient.newBuilder()
                .version(HttpClient.Version.HTTP_2)
                .build();

        this.unique_task_names = Arrays.asList(training_set.get_unique_Task_Name().keySet().toArray());

        String training_set_path = PythonAdapterConfiguration.RESOURCE_DIR + this.get_classifier_python_name() + "$training.xes";
        String test_set_path = PythonAdapterConfiguration.RESOURCE_DIR + this.get_classifier_python_name() + "$test.xes";

        training_set.export_XES(training_set_path);
        test_set.export_XES(test_set_path);

        Map<String, Object> data = new HashMap<>();

        Map<String, Object> configuration = new HashMap<>();
        for (Parameter_Communication_Wrapper conf_param :
                this.configurational_parameters()) {
            configuration.put(conf_param.text, conf_param.current_value);
        }

        data.put("given_name", this.get_classifier_python_name());
        data.put("classifier", this.classifier_class_python);

        data.put("train_path", training_set_path);
        data.put("test_path", test_set_path);
        data.put("parameters", configuration);
        data.put("ts", System.currentTimeMillis());

        String json = new ObjectMapper().writeValueAsString(data);

        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(PythonAdapterConfiguration.API_URL + "train-classifier"))
                .setHeader("User-Agent", "Camunda Python Adapter") // add request header
                .header("Content-Type", "application/json")
                .POST(HttpRequest.BodyPublishers.ofString(json))
                .build();

        HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());

        ObjectMapper mapper = new ObjectMapper();
        MapType type = mapper
                .getTypeFactory()
                .constructMapType(Map.class, String.class, Double.class);

        HashMap<String, Double> map = mapper.readValue(response.body(), type);
        return new PythonAdapterMetric(map.get("accuracy"));
    }

    public List<Classification> send_evaluate(Data_Set data_set) throws Exception {
        HttpClient httpClient = HttpClient.newBuilder()
                .version(HttpClient.Version.HTTP_2)
                .build();

        String predict_set_path = PythonAdapterConfiguration.RESOURCE_DIR + this.get_classifier_python_name() + "predict.xes";

        data_set.export_XES(predict_set_path);

        Map<String, Object> data = new HashMap<>();

        data.put("given_name", this.get_classifier_python_name());
        data.put("predict_path", predict_set_path);
        data.put("ts", System.currentTimeMillis());

        String json = new ObjectMapper().writeValueAsString(data);

        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(PythonAdapterConfiguration.API_URL + "predict-classifier"))
                .setHeader("User-Agent", "Camunda Python Adapter") // add request header
                .header("Content-Type", "application/json")
                .POST(HttpRequest.BodyPublishers.ofString(json))
                .build();

        HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());

        ObjectMapper mapper = new ObjectMapper();
        MapType type = mapper
                .getTypeFactory()
                .constructMapType(Map.class, String.class, Float.class);

        HashMap<String, Float> map = mapper.readValue(response.body(), type);
        return Arrays.asList(new Classification(map, PredictionType.ActivityPrediction));
    }

    private List<Classification> map_to_prediction(Map<String, Object> map) {
        List<Classification> classifications = new ArrayList<>();
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            Map<String, Object> probabilities = (Map<String, Object>) entry.getValue();
            HashMap<String, Float> probability_map = new HashMap<>();
            for (Object uid :
                    this.unique_task_names) {
                if (probabilities.containsKey(uid.toString())) {
                    probability_map.put(uid.toString(), Float.parseFloat(probabilities.get(uid.toString()).toString()));
                } else {
                    probability_map.put(uid.toString(), 0.0f);
                }
            }
            classifications.add(new Classification(probability_map, PredictionType.ActivityPrediction));
        }
        return classifications;
    }

}
