package uni_ko.PythonAdapter.MachineLearning.Metric;

import uni_ko.bpm.Machine_Learning.Metric;

import java.util.List;

public class PythonAdapterMetric extends Metric {

    private static final long serialVersionUID = 1229706138345590251L;

    protected Double accuracy = 0.0;

    public PythonAdapterMetric(Double accuracy) {
        this.accuracy = accuracy;
    }

    @Override
    public Double accuracy() {
        return this.accuracy;
    }

    @Override
    public Double MAE() {
        throw new UnsupportedOperationException("Python Adapter Metric does not store MAE.");
    }

    @Override
    public Double SSE() {
        throw new UnsupportedOperationException("Python Adapter Metric does store SSE.");
    }


    @Override
    public void update(List<Double[]> values) {
        throw new UnsupportedOperationException("Python Adapter Metric does not allow updating.");
    }

    @Override
    public int getNoObservations() {
        throw new UnsupportedOperationException("Python Adapter Metric does not store the number of observations.");
    }

}
