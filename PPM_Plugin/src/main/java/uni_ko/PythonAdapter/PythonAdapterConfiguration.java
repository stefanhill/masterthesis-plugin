package uni_ko.PythonAdapter;

public class PythonAdapterConfiguration {

    // Resource directory to transfer training logs followed with backslash
    public static String RESOURCE_DIR = "C:\\git\\masterthesis\\appdata\\temp\\";
    // API URL followed with backslash
    public static String API_URL = "http://127.0.0.1:5000/";

}
