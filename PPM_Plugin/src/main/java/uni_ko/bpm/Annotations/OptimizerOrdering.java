package uni_ko.bpm.Annotations;


import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;




@Target(ElementType.METHOD)
@Inherited
@Retention(RetentionPolicy.RUNTIME)
public @interface OptimizerOrdering {
	public enum OrderingOption{
		Low, 
		High;
	}
	OrderingOption best() default OrderingOption.Low;
}
