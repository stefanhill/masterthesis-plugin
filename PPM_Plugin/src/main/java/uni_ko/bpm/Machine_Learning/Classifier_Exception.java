package uni_ko.bpm.Machine_Learning;

public class Classifier_Exception extends Exception{

	public Classifier_Exception(){
		   super("Basic Classifier Exception");
	}
	public Classifier_Exception(String error){
		super(error);
	}

}
