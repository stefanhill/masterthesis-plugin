package uni_ko.bpm.Machine_Learning.WekaClassifier;

import uni_ko.JHPOFramework.Structures.Pair;
import uni_ko.bpm.Machine_Learning.PredictionType;
import weka.core.Attribute;

import java.util.HashMap;
import java.util.List;

public class FieldUtilities {

    public static HashMap<String, Pair<Class, Object>> replaceValue = new HashMap<>();
    static {
        replaceValue.put("task_uid", new Pair<>(String.class, "Unknown"));
        replaceValue.put("duration", new Pair<>(Double.class, 0.0));
        replaceValue.put("RiskQuadratic", new Pair<>(Double.class, 0.0));
        replaceValue.put("RiskLinear", new Pair<>(Double.class, 0.0));
        replaceValue.put("bloodPressure", new Pair<>(Double.class, 0.0));
        replaceValue.put("org:resource", new Pair<>(String.class, "Unknown"));
    }

    public static boolean isNominal(String field) {
        return !FieldUtilities.replaceValue.get(field).getFirst().getSuperclass().equals(Number.class);
    }

    public static boolean isNumeric(String field) {
        return !FieldUtilities.isNominal(field);
    }

    public static String getFieldByPredictionType(PredictionType pt) {
        switch (pt) {
            case ActivityPrediction:
                return "task_uid";
            case TimePrediction:
                return "duration";
            case RiskPrediction:
                return "RiskQuadratic";
            default:
                return null;
        }
    }

    public static Attribute createAttribute(String field, int index, List<String> nominalValues) {
        return (FieldUtilities.isNominal(field)) ? new Attribute(field + index, nominalValues) : new Attribute(field + index);
    }

    public static boolean isNominal(PredictionType pt) {
        return pt.equals(PredictionType.ActivityPrediction);
    }

    public static boolean isNumeric(PredictionType pt) {
        return pt.equals(PredictionType.TimePrediction) || pt.equals(PredictionType.RiskPrediction);
    }

}
