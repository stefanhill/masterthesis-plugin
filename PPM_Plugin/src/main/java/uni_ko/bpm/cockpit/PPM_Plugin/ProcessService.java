package uni_ko.bpm.cockpit.PPM_Plugin;

import org.camunda.bpm.engine.*;
import org.camunda.bpm.engine.repository.ProcessDefinition;

public class ProcessService {

    private static ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
    private static RepositoryService repositoryService = processEngine.getRepositoryService();
    public static TaskService taskService = processEngine.getTaskService();
    public static HistoryService historyService = processEngine.getHistoryService();
    
    public static ProcessDefinition getProcessDefinitionByResourceName(String resourceName) {
    	ProcessDefinition processDefinition = repositoryService.createProcessDefinitionQuery()
                .processDefinitionResourceName(resourceName)
                .list()
                .get(0);
        return processDefinition;
    }
}
