package uni_ko.bpm.cockpit.PPM_Plugin.resources.HyperOpt;

import java.util.List;
import java.util.Map;

import javax.ws.rs.POST;

import org.camunda.bpm.cockpit.plugin.resource.AbstractCockpitPluginResource;

import uni_ko.JHPOFramework.Communication.CommunicationData;
import uni_ko.JHPOFramework.Communication.CommunicationHandler;
import uni_ko.JHPOFramework.Structures.Triple;
import uni_ko.bpm.cockpit.PPM_Plugin.ProcessService;


public class TestListResource extends AbstractCockpitPluginResource {


    public TestListResource(String engineName) {
        super(engineName);
    }

    @POST
    public List<Triple<String, String, Integer>> getTestList(Map<String, Object> requestData) throws Exception {
        String processDefinitionId = ProcessService.getProcessDefinitionByResourceName((String) requestData.get("resourceName")).getId();
        CommunicationHandler h = CommunicationData.get_frh(processDefinitionId);
        return h.getAllSimulations();
    }
}
