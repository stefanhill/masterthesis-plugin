package uni_ko.bpm.Data_Management;

import org.camunda.bpm.engine.ProcessEngine;
import org.camunda.bpm.engine.ProcessEngines;
import org.camunda.bpm.engine.history.HistoricActivityInstance;
import org.camunda.bpm.engine.history.HistoricVariableInstance;
import org.camunda.bpm.engine.runtime.ActivityInstance;
import uni_ko.JHPOFramework.Structures.Pair;
import uni_ko.bpm.cockpit.PPM_Plugin.ProcessService;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


public class Data_Point implements Serializable {
    private static final long serialVersionUID = -6347081765393903530L;

    protected HashMap<String, Pair<Class<?>, Object>> dataValues = new HashMap<String, Pair<Class<?>, Object>>();


    public Data_Point() {
    }

    public Data_Point(HashMap<String, Pair<Class<?>, Object>> dataValues) {
        this.dataValues = dataValues;
    }

    public Data_Point(String connceptName) {
        this.dataValues.put("concept:name", new Pair<Class<?>, Object>(String.class, connceptName));
    }

    public Data_Point(HistoricActivityInstance historic_Activity_Instance, HashMap<String, Integer> unique_Task_Name) {
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        this.dataValues.put("concept:name", new Pair<Class<?>, Object>(String.class, historic_Activity_Instance.getActivityId()));
        this.dataValues.put("time:timestamp", new Pair<Class<?>, Object>(Date.class, historic_Activity_Instance.getStartTime()));
        if (historic_Activity_Instance.getDurationInMillis() != null) {
            this.dataValues.put("duration", new Pair<Class<?>, Object>(Integer.class, historic_Activity_Instance.getDurationInMillis().intValue()));
        } else {
            this.dataValues.put("duration", new Pair<Class<?>, Object>(Integer.class, 0));
        }
        if (historic_Activity_Instance.getAssignee() != null) {
            this.dataValues.put("org:resource", new Pair<Class<?>, Object>(String.class, historic_Activity_Instance.getAssignee()));
        } else {
            this.dataValues.put("org:resource", new Pair<Class<?>, Object>(String.class, ""));
        }
        List<HistoricVariableInstance> bloodPressure = ProcessService.historyService.createHistoricVariableInstanceQuery()
                .processInstanceId(historic_Activity_Instance.getProcessInstanceId())
                .variableName("bloodPressure")
                .list();
        if (!bloodPressure.isEmpty()) {
            this.dataValues.put("bloodPressure", new Pair<Class<?>, Object>(Double.class, bloodPressure.get(0).getValue()));
        } else {
            this.dataValues.put("bloodPressure", new Pair<Class<?>, Object>(Double.class, null));
        }

    }

    public Data_Point(ActivityInstance activity_Instance, HashMap<String, Integer> unique_Task_Name) {

        this.dataValues.put("concept:name", new Pair<Class<?>, Object>(String.class, activity_Instance.getActivityName()));
        this.dataValues.put("time:timestamp", new Pair<Class<?>, Object>(Date.class, LocalDateTime.now()));
        this.dataValues.put("duration", new Pair<Class<?>, Object>(Integer.class, 0));
        this.dataValues.put("org:resource", new Pair<Class<?>, Object>(String.class, ""));

    }


    public void putDataValue(String name, Class<?> type, Object value) {
        this.dataValues.put(name, new Pair<Class<?>, Object>(type, value));
    }

    List<String> dateNames = new ArrayList<String>();

    public void putDateValue(String name, Class<? extends Date> type, Object value) {
        if (dateNames.contains(name)) {
            Long duration = Math.abs(((Date) this.dataValues.get(name).getValue()).getTime() - ((Date) value).getTime());
            if (!this.dataValues.containsKey("duration")) {
                this.dataValues.put("duration", new Pair<Class<?>, Object>(Integer.class, duration.intValue()));
            }
            this.dataValues.put(name + "_Duration", new Pair<Class<?>, Object>(Long.class, duration));
        }
        this.dataValues.put(name, new Pair<Class<?>, Object>(type, value));
        dateNames.add(name);
    }

    public HashMap<String, Pair<Class<?>, Object>> getDataValues() {
        return this.dataValues;
    }
}
