package uni_ko.bpm.Tests;

import org.camunda.bpm.engine.HistoryService;
import org.camunda.bpm.engine.ProcessEngine;
import org.camunda.bpm.engine.ProcessEngines;
import org.camunda.bpm.engine.RepositoryService;
import org.camunda.bpm.engine.repository.ProcessDefinition;
import uni_ko.bpm.Data_Management.Data_Reader.Data_Reader;
import uni_ko.bpm.Data_Management.Data_Reader.History_Reader;

import java.util.List;

public class HistoryTesting {

    public static void main(String[] args) {
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        RepositoryService repositoryService = processEngine.getRepositoryService();
        ProcessDefinition processDefinition = repositoryService.createProcessDefinitionQuery()
                .processDefinitionKey("sid-11167e06-c45d-4370-b356-4921b2398414")
                .singleResult();
        System.out.println("Runtime running");
        HistoryService historyService = processEngine.getHistoryService();
        List elements = historyService.createHistoricProcessInstanceQuery()
                .processDefinitionId("sid-11167e06-c45d-4370-b356-4921b2398414")
                .finished()
                .list();
        Data_Reader dr = new History_Reader(processDefinition.getId());
        System.out.println("fini");
    }
}
