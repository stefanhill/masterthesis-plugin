# Camunda PPM
This tool is a plugin for [Camunda](https://camunda.com), which adds the functionality of Predictive Process Monitoring to the Camunda framework. This includes the following features:

*   Create Learner, including next activity, time and risk prediction
*   Train it on internal Camunda logs or upload a XES file
*   Create multiple versions of a learner
*   Automatically find the best Hyper Parameters for your setup


## Installation
There are two different ways to install the plugin. You can either run it from your IDE (Spring-Boot) or you can create a plugin file and include it into an installed camunda instance.

__Run it in your IDE (Spring-Boot)__

1.  Clone the repository into your IDE as a maven project
2.  Build the pom.xml in the root folder by executing ``mvn clean install`` in the terminal or through your IDE
3.  Start Camunda by executing ``Camunda_Starter\Application.java``


__Plugin file__

1.  Download or clone the repository and open it in your IDE
2.  Create a .jar file (in Eclipse, rightclick on your project and export it)
3.  Place the .jar file in ``camunda-bpm-tomcat-[xxx]\server\apache-tomcat-[xxx]\webapps\camunda\WEB-INF\lib\``
4.  In order to run the plugin, change ``MaxPermSize`` and ``PermSize`` from 256m to 2048m in ``camunda-bpm-tomcat-[xxx]\server\apache-tomcat-[xxx]\bin\setenv.[bat|sh]``
5.  (Re)start Camunda


## Usage

### General usage

1.  Inside the Cockpit Environment, navigate to your process.
2.  Click 'more' -> Deployments<br>
    ![1](Resources/screenshots/1.JPG)<br>
    ![2](Resources/screenshots/2.JPG)
3.  Select a bpmn, then navigate to the PPM Creation Tab<br>
    ![3](Resources/screenshots/3.JPG)
4.  A Tab displaying all Classifiers you can choose from will open<br>
    ![4](Resources/screenshots/4.JPG)
5.  Select and name your Classifier, set the Prediction Types and if you know what you're doing,**<br> **you can change the Classifier parameters which are set to a reasonable value by default. Finally, click on the 'Create' Button on the right<br>
    ![5](Resources/screenshots/5.JPG)


6.  Navigate to the PPM Training Tab. Now you have to choose, if you want to train the Classifier with data from a specified time frame or from a XES-log File.
    In case you've chosen to train from a time frame, you'll have to use the two calendar-widgets to specify the time frame and check the little box 'History Time Frame'<br>
    ![6](Resources/screenshots/6.JPG)


7.  Click 'Start' to start training your Classifier. This may take a while.
8.  After the training is done, you'll be able to see the Classifiers' accuracy.<br>
    ![7](Resources/screenshots/7.JPG)
9.  Navigate to the PPM Runtime Tab, and set the Classifier to public, by clicking 👁️
10. You can set the Classifier as default Classifier, by clicking ⭐. Every default Classifier must be public.
    (Please note that inside the instance-view, you'll later only be able to choose from a list of **public** Classifiers. If you don't set a Classifier as public, it won't get listed as available Classifier in the instance view)<br>
    ![8](Resources/screenshots/8.JPG)
11.  You're almost done! The Prediction Tab should now be visible from the Instance-View of your Process. Let's have a look.<br>
    ![8-5](Resources/screenshots/8-5.JPG)
12. And, as expected, we can see that our Classifier has made a prediction. In this example the Classifier doesn't predict time, but this isn't a problem, just check the box 'TimePrediction' the next time you create a new Classifier.<br>
![9](Resources/screenshots/9.JPG)



Certain Classifiers are fit for a certain task. A Classifier performing well in predicting time may not be performing as well in predicting the next activity. See [our lab report](Resources/report.pdf) for more detailed information and suggestions.

### Hyper parameter optimization

In order to find the best hyper parameter configuration, the plugin provides a built-in optimizer, which is located in the "PPM Find" tab.
![HPO-1](Resources/screenshots/hpo-1.png)

13. First, add a new test run, give it a name, set the training data as in "PPM Train" before, and select the Classifier that will be in the game. For each Classifier, you can set checkboxes for categorical parameters or ranges for number parameters. The recommended values are already inserted for you.
![HPO-2](Resources/screenshots/hpo-2.png)
14. Then, select the metric to check the quality of a given model. "accuracy" often suffices, but you can also go for error rates or model dependent stuff like AIC if you want to. Select your optimizer and set the parameters and let the computer do the rest for you. Details about the optimizer can be found in [our paper](Resources/paper.pdf).
![HPO-3](Resources/screenshots/hpo-3.png)
15. After some time (and clicking on "Reload"), you will see the list of running and finished tests. Then, click on your test to see the results.
![HPO-4](Resources/screenshots/hpo-4.png)
16. If there is Classifier with high accuracy around, take it, give it a name, select the "trained" option (when you do not want to train again on other data) and "Create Configuration". Your classifier will appear in the "PPM Runtime" tab and is ready to be used for predictions and more sophisticated stuff like merging.
![HPO-5](Resources/screenshots/hpo-5.png)